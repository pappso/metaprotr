# metaprotr 1.3.1
* Addition of arguments to modify the axis and legend font size in the functions: plot_intensities, plot_fulltaxo, plot_stackedtaxo and plot_pietaxo

# metaprotr 1.3.0
* Addition of new argument to the function plot_dendocluster to take over labels on plot
* Sanitizer of inputs added to the load_protspeps function 

# metaprotr 1.2.2

* Addition of four datasets to test the different functions.
* Verification that graphical setting are restored upon exit of the functions.
* The statements T/F were changed to TRUE/FALSE in all the functions.
* The examples of the functions write data on tempdir(). 
* To compile with the CRAN policies, we explicitly ask the user whether a file (csv or pdf) should be created on the working directory when a given function is called.

# metaprotr 1.2.1

* Addition of `NEWS.md` file to track changes to the package.



